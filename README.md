# ABFA
ABFA project belongs to a municipality contractor([Naghshe negar](https://rasm.io/company/10260279362/%D9%85%D9%87%D9%86%D8%AF%D8%B3%DB%8C%D9%86%20%D9%85%D8%B4%D8%A7%D9%88%D8%B1%20%D9%86%D9%82%D8%B4%D9%87%20%D8%A8%D8%B1%D8%AF%D8%A7%D8%B1%DB%8C%20%D9%86%D9%82%D8%B4%D9%87%20%D9%86%DA%AF%D8%A7%D8%B1/)),
In order to create edit delete forms of manholes and pipes and export their forms as csv files.
In project outlook we're going to make it online.

## Screenshot
<p align="center"><img align="center" src="./Screenshot_2020-06-29-23-02-23-413_com.idea.abfa.jpg?raw=true." alt="Spire the Hare" title="ScreenShot1" width="900px"></p>

## Get started

```
git clone https://gitlab.com/future-ideas/abfa.git
```

## Built With

* Kotlin & Jetpack

## Contributing

* Ensure any install or build dependencies are removed before the end of the layer when doing a build.
* Update the README.md with details of changes to the interface and anything else.
* Increase the version numbers in any examples files and the README.md to the new version that this Pull Request would represent.
* Your pull request will be checked and merged(If be verified).

## Author

* **Amir Muhammad Karimi** - [AMK9978](https://github.com/amk9978)

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc