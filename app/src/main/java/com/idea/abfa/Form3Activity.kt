package com.idea.abfa

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button

import androidx.appcompat.app.AppCompatActivity

import cn.pedant.SweetAlert.SweetAlertDialog
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Manhole
import com.idea.abfa.repositories.ManholeRepository
import kotlinx.android.synthetic.main.activity_form3.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class Form3Activity : AppCompatActivity() {
    private var manhole: Manhole? = null

    private fun getGateStatus(): String {
        return when {
            radioBroken!!.isChecked -> radioBroken.text.toString()
            radioLower!!.isChecked -> radioLower.text.toString()
            radioHigher!!.isChecked -> radioHigher.text.toString()
            radioAbsence!!.isChecked -> radioAbsence.text.toString()
            radioInvisible!!.isChecked -> radioInvisible.text.toString()
            else -> radioNormal.text.toString()
        }
    }

    private fun getStairs(): String {
        return if (radioStairYes!!.isChecked) {
            radioStairYes.text.toString()
        } else {
            radioStairNo.text.toString()
        }
    }

    private fun getType(): String {
        return if (radioIron!!.isChecked) {
            radioIron.text.toString()
        } else {
            radioMetal.text.toString()
        }
    }

    private fun getRainPermeation(): String {
        return if (radioRainYes!!.isChecked) {
            radioRainYes.text.toString()
        } else {
            radioRainNo.text.toString()
        }
    }

    private fun getNeedRestoration(): String {
        return if (radioRestoration!!.isChecked) {
            radioRestoration.text.toString()
        } else {
            radioNotRestoration.text.toString()
        }
    }

    private fun getGateFrameStatus(): String {
        return when {
            radioBrokenFrame!!.isChecked -> {
                radioBrokenFrame.text.toString()
            }
            radioFrameAbsence!!.isChecked -> {
                radioFrameAbsence.text.toString()
            }
            else -> {
                radioNormalFrame.text.toString()
            }
        }
    }

    private fun getFrameKeeper(): String {
        return when {
            radioKeeperConcrete!!.isChecked -> {
                radioKeeperConcrete.text.toString()
            }
            radioAsphalt!!.isChecked -> {
                radioAsphalt.text.toString()
            }
            radioPaving!!.isChecked -> {
                radioPaving.text.toString()
            }
            radioSoul!!.isChecked -> {
                radioSoul.text.toString()
            }
            else -> {
                radioKeeperElse.text.toString()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form3)
        val next = findViewById<Button>(R.id.next)
        val previous = findViewById<Button>(R.id.previous)
        manhole = intent.extras!!.get("manhole") as Manhole?
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)


        when (manhole?.frameKeeper) {
            radioKeeperConcrete.text!!.toString() -> {
                radioKeeperConcrete.isChecked = true
                radioAsphalt.isChecked = false
                radioPaving.isChecked = false
                radioSoul.isChecked = false
                radioKeeperElse.isChecked = false
            }
            radioAsphalt.text!!.toString() -> {
                radioKeeperConcrete.isChecked = true
                radioAsphalt.isChecked = true
                radioPaving.isChecked = false
                radioSoul.isChecked = false
                radioKeeperElse.isChecked = false
            }
            radioPaving.text!!.toString() -> {
                radioKeeperConcrete.isChecked = false
                radioAsphalt.isChecked = false
                radioPaving.isChecked = true
                radioSoul.isChecked = false
                radioKeeperElse.isChecked = false
            }
            radioSoul.text!!.toString() -> {
                radioKeeperConcrete.isChecked = false
                radioAsphalt.isChecked = false
                radioPaving.isChecked = false
                radioSoul.isChecked = true
                radioKeeperElse.isChecked = false
            }
            else -> {
                radioKeeperConcrete.isChecked = false
                radioAsphalt.isChecked = false
                radioPaving.isChecked = false
                radioSoul.isChecked = false
                radioKeeperElse.isChecked = true
            }
        }

        when (manhole?.gateFrameStatus) {
            radioBrokenFrame.text!!.toString() -> {
                radioBrokenFrame.isChecked = true
                radioNormalFrame.isChecked = false
                radioFrameAbsence.isChecked = false
            }
            radioNormalFrame.text!!.toString() -> {
                radioBrokenFrame.isChecked = false
                radioNormalFrame.isChecked = true
                radioFrameAbsence.isChecked = false
            }
            else -> {
                radioBrokenFrame.isChecked = false
                radioNormalFrame.isChecked = false
                radioFrameAbsence.isChecked = true
            }
        }

        when (manhole?.needRestoration) {
            radioRestoration.text!!.toString() -> {
                radioRestoration.isChecked = true
                radioNotRestoration.isChecked = false
            }
            else -> {
                radioRestoration.isChecked = false
                radioNotRestoration.isChecked = true
            }
        }
        when (manhole?.rainPenetration) {
            radioRainYes.text!!.toString() -> {
                radioRainYes.isChecked = true
                radioRainNo.isChecked = false
            }
            else -> {
                radioRainYes.isChecked = false
                radioRainNo.isChecked = true
            }
        }
        when (manhole?.stairType) {
            radioIron.text!!.toString() -> {
                radioIron.isChecked = true
                radioMetal.isChecked = false
            }
            else -> {
                radioIron.isChecked = false
                radioMetal.isChecked = true
            }
        }
        when (manhole?.stairs) {
            radioStairYes.text!!.toString() -> {
                radioStairYes.isChecked = true
                radioStairNo.isChecked = false
            }
            else -> {
                radioStairYes.isChecked = false
                radioStairNo.isChecked = true
            }
        }

        when (manhole?.gateStatus) {
            radioBroken.text!!.toString() -> {
                radioBroken.isChecked = true
                radioLower.isChecked = false
                radioHigher.isChecked = false
                radioAbsence.isChecked = false
                radioInvisible.isChecked = false
                radioNormal.isChecked = false
            }
            radioLower.text!!.toString() -> {
                radioBroken.isChecked = false
                radioLower.isChecked = true
                radioHigher.isChecked = false
                radioAbsence.isChecked = false
                radioInvisible.isChecked = false
                radioNormal.isChecked = false
            }
            radioHigher.text!!.toString() -> {
                radioBroken.isChecked = false
                radioLower.isChecked = false
                radioHigher.isChecked = true
                radioAbsence.isChecked = false
                radioInvisible.isChecked = false
                radioNormal.isChecked = false
            }
            radioAbsence.text!!.toString() -> {
                radioBroken.isChecked = false
                radioLower.isChecked = false
                radioHigher.isChecked = false
                radioAbsence.isChecked = true
                radioInvisible.isChecked = false
                radioNormal.isChecked = false
            }
            radioInvisible.text!!.toString() -> {
                radioBroken.isChecked = false
                radioLower.isChecked = false
                radioHigher.isChecked = false
                radioAbsence.isChecked = false
                radioInvisible.isChecked = true
                radioNormal.isChecked = false
            }
            else -> {
                radioBroken.isChecked = false
                radioLower.isChecked = false
                radioHigher.isChecked = false
                radioAbsence.isChecked = false
                radioInvisible.isChecked = false
                radioNormal.isChecked = true
            }
        }

        stair_number.setText(manhole?.stairNumber)
        holes_number.setText(manhole?.holesNumber)

        next.setOnClickListener {
            if (!checkFields()) {
                showErrorDialog()
                return@setOnClickListener
            }
            manhole?.frameKeeper = getFrameKeeper()
            manhole?.gateFrameStatus = getGateFrameStatus()
            manhole?.needRestoration = getNeedRestoration()
            manhole?.rainPenetration = getRainPermeation()
            manhole?.stairType = getType()
            manhole?.stairs = getStairs()
            manhole?.gateStatus = getGateStatus()
            manhole?.holesNumber = holes_number.text.toString()
            manhole?.stairNumber = stair_number.text.toString()

            manhole?.let { it1 -> editManhole(it1) }
            val intent = Intent(this, Form4Activity::class.java)
            intent.putExtra("manhole", manhole)
            startActivity(intent)
        }

        previous.setOnClickListener {
            val intent = Intent(this, Form2Activity::class.java)
            intent.putExtra("manhole", manhole)
            startActivity(intent)
        }
    }

    private fun editManhole(manhole: Manhole) {
        GlobalScope.async {
            val repo = ManholeRepository(MyRoomDB.getDatabase(this@Form3Activity).manholeDAO())
            repo.update(manhole)
        }
    }
    private fun showErrorDialog() {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        sweetAlertDialog.setTitle("مشکل!")
        sweetAlertDialog.contentText = "همه بخش های لازم را پر کنید"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { sweetAlertDialog.dismiss() }
    }

    private fun checkFields(): Boolean {
        return true
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.history_more, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection

        return super.onOptionsItemSelected(item)
    }

    private fun history() {
        val intent = Intent(this, HistoryActivity::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
