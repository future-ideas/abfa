package com.idea.abfa.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class SqliteHelper(context: Context?, name: String?, factory: SQLiteDatabase.CursorFactory?, version: Int)
    : SQLiteOpenHelper(context, name, factory, version) {

    companion object {
        public const val MANHOLES_TABLE_NAME = "MANHOLES"
        public const val PIPES_TABLE_NAME = "PIPES"
        public const val USER_NAME = "نام و نام خانوادگی تکمیل کننده"
        public const val MUN_REGION = "کد منطقه شهرداری"
        public const val PIPE_CODE = "کد لوله"
        public const val UPPER_MANHOLE_CODE = "کد منهول بالادست"
        public const val LOWER_MANHOLE_CODE = "کد منهول پایین دست"
        public const val PIPE_DIAMETER = "کد منهول پایین دست"

    }


    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("create table if not exists " + MANHOLES_TABLE_NAME + "( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + USER_NAME + " TEXT, " + MUN_REGION + " INTEGER)")

        db?.execSQL("create table if not exists " + PIPES_TABLE_NAME + "( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + PIPE_CODE + " INTEGER, " + UPPER_MANHOLE_CODE + " INTEGER, " + LOWER_MANHOLE_CODE +
                " INTEGER, " + PIPE_DIAMETER + " INTEGER)")
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        db?.execSQL("drop table if exists $MANHOLES_TABLE_NAME")
        db?.execSQL("drop table if exists $PIPES_TABLE_NAME")
        onCreate(db)
    }
}