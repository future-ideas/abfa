package com.idea.abfa.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.idea.abfa.models.User

@Dao
interface UserDAO {
    @Query("SELECT * from users_table")
    fun getUsers(): LiveData<List<User>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(user: User)

    @Query("DELETE FROM users_table")
    suspend fun deleteAll()
}