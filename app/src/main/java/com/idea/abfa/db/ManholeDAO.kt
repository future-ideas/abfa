package com.idea.abfa.db

import androidx.room.*
import com.idea.abfa.models.Manhole
import com.idea.abfa.models.Pipe

@Dao
interface ManholeDAO {
    @Query("SELECT * from manholes_table")
    fun getManholes(): List<Manhole>

    @Query("SELECT * from pipes_table where manhole_id=:manholeId")
    fun getPipes(manholeId: Int): List<Pipe>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(manhole: Manhole): Long

    @Query("DELETE FROM manholes_table")
    suspend fun deleteAll()

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(manhole: Manhole)

    @Delete
    suspend fun delete(manhole: Manhole)
}