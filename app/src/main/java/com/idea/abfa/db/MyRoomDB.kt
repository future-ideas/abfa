package com.idea.abfa.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.idea.abfa.models.Manhole
import com.idea.abfa.models.Pipe
import com.idea.abfa.models.User

@Database(entities = [Manhole::class, User::class, Pipe::class], version = 2, exportSchema = false)
abstract class MyRoomDB : RoomDatabase() {
    abstract fun manholeDAO(): ManholeDAO
    abstract fun userDAO(): UserDAO
    abstract fun pipeDAO(): PipeDAO

    companion object {
        // Singleton prevents multiple instances of database opening at the same time.
        @Volatile
        private var INSTANCE: MyRoomDB? = null

        fun getDatabase(context: Context): MyRoomDB {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext, MyRoomDB::class.java,
                        "abfa_database").build()
                INSTANCE = instance
                return instance
            }
        }
    }
}