package com.idea.abfa.db

import androidx.room.*
import com.idea.abfa.models.Manhole
import com.idea.abfa.models.Pipe

@Dao
interface PipeDAO {
    @Query("SELECT * from pipes_table")
    fun getPipes(): List<Pipe>

    @Query("SELECT * from manholes_table where id =:manholeId")
    fun getManhole(manholeId: Int): Manhole

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(pipe: Pipe): Long

    @Query("DELETE FROM pipes_table")
    suspend fun deleteAll()

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(pipe: Pipe)

    @Delete
    suspend fun delete(pipe: Pipe)
}