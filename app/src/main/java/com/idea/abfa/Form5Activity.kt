package com.idea.abfa

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.bumptech.glide.Glide
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Manhole
import com.idea.abfa.repositories.ManholeRepository
import kotlinx.android.synthetic.main.activity_form5.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class Form5Activity : AppCompatActivity() {


    private var manhole: Manhole? = null

    private val galleryReqCode: Int = 100
    private val permissionCode: Int = 100

    private fun askPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED)) { // Should we show an explanation?
                shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE) // Explain to the user why we need to read the contacts
                shouldShowRequestPermissionRationale(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) // Explain to the user why we need to read the contacts
                shouldShowRequestPermissionRationale(
                        Manifest.permission.CAMERA) // Explain to the user why we need to read the contacts
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA),
                        permissionCode)
                // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
// app-defined int constant that should be quite unique

            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (permissionCode == requestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) { //permission from popup granted
                pickImageFromGallery()
            }
        }
    }

    private fun saveImageToInternalStorage(bitmap: Bitmap) {
        val mainFile = "ManholeInspection"
        val f = File(Environment.getExternalStorageDirectory(), mainFile)
        if (!f.exists()) {
            f.mkdir()
        }
        val myPath = f.path + File.separator + "output" + "_" + getDateAndTime()[0] + "_" + getDateAndTime()[1] + ".png"
        File(myPath).parentFile?.mkdirs()
        val out = FileOutputStream(myPath)
        try {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        manhole?.photoName = myPath
    }

    private fun getDateAndTime(): Array<String> {
        val cal = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("yyyy_MM_dd")
        val hour = cal.get(Calendar.HOUR_OF_DAY)
        var t = hour.toString() + "_"
        val min = cal.get(Calendar.MINUTE)
        t += if (min < 10) {
            if (min == 0) {
                "00"
            } else {
                "0$min"
            }
        } else {
            "$min"
        }
        return arrayOf(simpleDateFormat.format(cal.time), t)
    }

    private fun pickImageFromGallery() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, galleryReqCode)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == galleryReqCode) {
            showImage(data?.extras?.get("data"))
            saveImageToInternalStorage(data?.extras?.get("data") as Bitmap)
            return
        }
    }

    private fun showImage(uri: Any?) {
        warning_txt.visibility = View.GONE
        add_image.visibility = View.GONE
        upload.visibility = View.VISIBLE
        Glide.with(this).load(uri).into(upload)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form5)
        val next = findViewById<Button>(R.id.next)
        val previous = findViewById<Button>(R.id.previous)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        manhole = intent.extras!!.get("manhole") as Manhole?
        askPermissions()
        add_image.setOnClickListener { pickImageFromGallery() }
        upload.setOnClickListener { pickImageFromGallery() }
        if (manhole?.photoName != "") {
            showImage(manhole?.photoName)
        }
        if (manhole?.needsToPlaster == radioNormal.text.toString()) {
            radioNormal.isChecked = true
            radioDefective.isChecked = false
        } else {
            radioNormal.isChecked = false
            radioDefective.isChecked = true
        }

        when (manhole?.smellStatus) {
            ra41_1.text!!.toString() -> {
                ra41_1!!.isChecked = true
                ra41_2.isChecked = false
                ra41_3.isChecked = false
            }
            ra41_2.text!!.toString() -> {
                ra41_1!!.isChecked = false
                ra41_2.isChecked = true
                ra41_3.isChecked = false
            }
            else -> {
                ra41_1!!.isChecked = false
                ra41_2.isChecked = false
                ra41_3.isChecked = true
            }
        }

        when (manhole?.flowStatus) {
            ra42_1.text!!.toString() -> {
                ra42_1!!.isChecked = true
                ra42_2.isChecked = false
                ra42_3.isChecked = false
            }
            ra42_2.text!!.toString() -> {
                ra42_1!!.isChecked = false
                ra42_2.isChecked = true
                ra42_3.isChecked = false
            }
            else -> {
                ra42_1!!.isChecked = false
                ra42_2.isChecked = false
                ra42_3.isChecked = true
            }
        }

        when (manhole?.vermin) {
            ra43_1.text!!.toString() -> {
                ra43_1!!.isChecked = true
                ra43_2.isChecked = false
                ra43_3.isChecked = false
            }
            ra43_2.text!!.toString() -> {
                ra43_1!!.isChecked = false
                ra43_2.isChecked = true
                ra43_3.isChecked = false
            }
            else -> {
                ra43_1!!.isChecked = false
                ra43_2.isChecked = false
                ra43_3.isChecked = true
            }
        }

        when (manhole?.rootPenetration) {
            ra45_1.text!!.toString() -> {
                ra45_1!!.isChecked = true
                ra45_2.isChecked = false
            }
            else -> {
                ra45_1!!.isChecked = false
                ra45_2.isChecked = true
            }
        }

        when (manhole?.waterPenetration) {
            ra46_1.text!!.toString() -> {
                ra46_1!!.isChecked = true
                ra46_2.isChecked = false
            }
            else -> {
                ra46_1!!.isChecked = false
                ra46_2.isChecked = true
            }
        }
        when (manhole?.heavyFailure) {
            ra40_1.text!!.toString() -> {
                ra40_1!!.isChecked = true
                ra40_2.isChecked = false
            }
            else -> {
                ra40_1!!.isChecked = false
                ra40_2.isChecked = true
            }
        }

        ch44_3.setOnClickListener {
            elseName.isEnabled = ch44_3.isChecked
        }

        val manhole1 = manhole?.concreteInsideStatus?.split("|")
        ch39_1.isChecked = false
        ch39_2.isChecked = false
        ch39_3.isChecked = false
        ch39_4.isChecked = false
        ch44_1.isChecked = false
        ch44_2.isChecked = false
        ch44_3.isChecked = false
        ch47_1.isChecked = false
        ch47_2.isChecked = false
        ch47_3.isChecked = false
        ch47_4.isChecked = false
        ch48_1.isChecked = false
        ch48_2.isChecked = false
        ch48_3.isChecked = false
        ch49_1.isChecked = false
        ch49_2.isChecked = false
        ch49_3.isChecked = false
        ch49_4.isChecked = false
        elseName.setText("")

        if (manhole1?.contains(ch39_1.text.toString())!!)
            ch39_1.isChecked = true
        if (manhole1.contains(ch39_2.text.toString()))
            ch39_2.isChecked = true
        if (manhole1.contains(ch39_3.text.toString()))
            ch39_3.isChecked = true
        if (manhole1.contains(ch39_4.text.toString()))
            ch39_4.isChecked = true

        val manhole2 = manhole?.sediments?.split("|")
        if (manhole2?.contains(ch47_1.text.toString())!!)
            ch47_1.isChecked = true
        if (manhole2.contains(ch47_2.text.toString()))
            ch47_2.isChecked = true
        if (manhole2.contains(ch47_3.text.toString()))
            ch47_3.isChecked = true
        if (manhole2.contains(ch47_4.text.toString()))
            ch47_4.isChecked = true

        val manhole3 = manhole?.verminNames?.split("|")
        if (manhole3?.contains(ch44_1.text.toString())!!)
            ch44_1.isChecked = true
        if (manhole3.contains(ch44_2.text.toString()))
            ch44_2.isChecked = true
        if (manhole3.contains(ch44_3.text.toString())) {
            ch44_3.isChecked = true
            elseName.isEnabled = true
            val elseN = manhole?.verminNames?.split(":")
            if (elseN != null) {
                if (elseN.size > 1) {
                    elseName.setText(elseN[1])
                }
            }
        }
        val manhole4 = manhole?.obstacle?.split("|")
        if (manhole4?.contains(ch48_1.text.toString())!!)
            ch48_1.isChecked = true
        if (manhole4.contains(ch48_2.text.toString()))
            ch48_2.isChecked = true
        if (manhole4.contains(ch48_3.text.toString())) {
            ch48_3.isChecked = true
        }

        val manhole5 = manhole?.facilities?.split("|")
        if (manhole5?.contains(ch49_1.text.toString())!!)
            ch49_1.isChecked = true
        if (manhole5.contains(ch49_2.text.toString()))
            ch49_2.isChecked = true
        if (manhole5.contains(ch49_3.text.toString())) {
            ch49_3.isChecked = true
        }
        if (manhole5.contains(ch49_4.text.toString())) {
            ch49_4.isChecked = true
        }

        ed48.setText(manhole?.description)
        next.setOnClickListener {
            if (!checkFields()) {
                showErrorDialog()
                return@setOnClickListener
            }
            manhole?.needsToPlaster = getKeeperPlaster()

            var txt = ""
            if (ch39_1.isChecked)
                txt += ch39_1.text.toString() + "|"
            if (ch39_2.isChecked)
                txt += ch39_2.text.toString() + "|"
            if (ch39_3.isChecked)
                txt += ch39_3.text.toString() + "|"
            if (ch39_4.isChecked)
                txt += ch39_4.text.toString() + "|"

            var txt2 = ""
            if (ch47_1.isChecked)
                txt2 += ch47_1.text.toString() + "|"
            if (ch47_2.isChecked)
                txt2 += ch47_2.text.toString() + "|"
            if (ch47_3.isChecked)
                txt2 += ch47_3.text.toString() + "|"
            if (ch47_4.isChecked)
                txt2 += ch47_4.text.toString() + "|"

            var txt3 = ""
            if (ch44_1.isChecked)
                txt3 += ch44_1.text.toString() + "|"
            if (ch44_2.isChecked)
                txt3 += ch44_2.text.toString() + "|"
            if (ch44_3.isChecked) {
                txt3 += ch44_3.text.toString() + "|"
                txt3 += ":" + elseName.text.toString()
            }
            var txt4 = ""
            if (ch48_1.isChecked)
                txt4 += ch48_1.text.toString() + "|"
            if (ch48_2.isChecked)
                txt4 += ch48_2.text.toString() + "|"
            if (ch48_3.isChecked) {
                txt4 += ch48_3.text.toString() + "|"
            }
            var txt5 = ""
            if (ch49_1.isChecked)
                txt5 += ch49_1.text.toString() + "|"
            if (ch49_2.isChecked)
                txt5 += ch49_2.text.toString() + "|"
            if (ch49_3.isChecked) {
                txt5 += ch49_3.text.toString() + "|"
            }
            if (ch49_4.isChecked) {
                txt5 += ch49_4.text.toString() + "|"
            }
            manhole?.smellStatus = getSmellStatus()
            manhole?.flowStatus = getFlowStatus()
            manhole?.vermin = getVermin()
            manhole?.verminNames = txt3
            manhole?.rootPenetration = getRootPenetration()
            manhole?.waterPenetration = getWaterPenetration()
            manhole?.sediments = txt2
            manhole?.concreteInsideStatus = txt
            manhole?.obstacle = txt4
            manhole?.facilities = txt5
            manhole?.heavyFailure = getHeavyFailure()
            manhole?.description = ed48.text.toString()

            manhole?.let { it1 -> editManhole(it1) }
            val intent = Intent(this, ProfileActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        previous.setOnClickListener {
            val intent = Intent(this, Form4Activity::class.java)
            intent.putExtra("manhole", manhole)
            startActivity(intent)
        }
    }

    private fun editManhole(manhole: Manhole) {
        GlobalScope.async {
            val repo = ManholeRepository(MyRoomDB.getDatabase(this@Form5Activity).manholeDAO())
            repo.update(manhole)
        }
    }

    private fun getWaterPenetration(): String {
        return when {
            ra46_1.isChecked -> ra46_1.text.toString()
            else -> ra46_2.text.toString()
        }
    }

    private fun getRootPenetration(): String {
        return when {
            ra45_1.isChecked -> ra45_1.text.toString()
            else -> ra45_2.text.toString()
        }
    }

    private fun getVermin(): String {
        return when {
            ra43_1.isChecked -> ra43_1.text.toString()
            ra43_2.isChecked -> ra43_2.text.toString()
            else -> ra43_3.text.toString()
        }
    }

    private fun getFlowStatus(): String {
        return when {
            ra42_1.isChecked -> ra42_1.text.toString()
            ra42_2.isChecked -> ra42_2.text.toString()
            else -> ra42_3.text.toString()
        }
    }


    private fun getSmellStatus(): String {
        return when {
            ra41_1.isChecked -> ra41_1.text.toString()
            ra41_2.isChecked -> ra41_2.text.toString()
            else -> ra41_3.text.toString()
        }
    }

    private fun getHeavyFailure(): String {
        return if (ra40_1.isChecked) {
            ra40_1.text.toString()
        } else {
            ra40_2.text.toString()
        }
    }

    private fun getKeeperPlaster(): String {
        return if (radioNormal.isChecked) {
            radioNormal.text.toString()
        } else {
            radioDefective.text.toString()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.history_more, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection

        return super.onOptionsItemSelected(item)
    }

    private fun history() {
        val intent = Intent(this, HistoryActivity::class.java)
        startActivity(intent)
    }


    private fun showErrorDialog() {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        sweetAlertDialog.setTitle("مشکل!")
        sweetAlertDialog.contentText = "همه بخش های لازم را پر کنید"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { sweetAlertDialog.dismiss() }
    }

    private fun checkFields(): Boolean {
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
