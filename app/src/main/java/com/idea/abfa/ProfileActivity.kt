package com.idea.abfa

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Manhole
import com.idea.abfa.models.Pipe
import com.idea.abfa.repositories.ManholeRepository
import com.idea.abfa.repositories.PipeRepository
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import net.ozaydin.serkan.easy_csv.EasyCsv
import net.ozaydin.serkan.easy_csv.FileCallback
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.reflect.full.memberProperties

class ProfileActivity : AppCompatActivity(), LocationListener {

    private var TAG_CODE_PERMISSION_LOCATION: Int = 105
    private lateinit var pipes : List<Pipe>
    private val permissionCode: Int = 100
    private lateinit var sweetAlertDialog: SweetAlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)

        ActivityCompat.requestPermissions(this, arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        ), TAG_CODE_PERMISSION_LOCATION)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {

            val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 5000L, 10f, this)
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0f, this)
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, this)
        }


        manholeAdding.setOnClickListener {
            val intent = Intent(this, Form1Activity::class.java)
            startActivity(intent)
        }
        manholes_recycler.setOnClickListener {
            val intent = Intent(this, ManholesActivity::class.java)
            startActivity(intent)
        }
        if (getAdmin()) {
            exportBtn.visibility = View.VISIBLE
        }
        exportBtn.setOnClickListener {
            saveData()
        }
    }

    private fun getAdmin(): Boolean {
        val shared = getSharedPreferences("pref", Context.MODE_PRIVATE)
        return shared.getBoolean("admin", false)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.history_more, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        if (item.itemId == R.id.history) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun export(manholes: List<Manhole>) {
        val easyCsv = EasyCsv(this)
        val headerList = ArrayList<String>()
        val dataList = ArrayList<String>()
        for (name in Manhole::class.memberProperties) {
            if (name.name !in Manhole.dic())
                continue
            Log.i("TAG", name.name)
            headerList.add(Manhole.dic()[name.name] + ",")
        }

        Log.i("TAG", "HeaderList size: " + headerList.size)
        headerList.add("-")
        for ((i, manhole) in manholes.withIndex()) {
            for (name in Manhole::class.memberProperties) {
                if (name.name !in Manhole.dic()) {
                    continue
                }
                if (name.name == "address") {
                    dataList.add(manhole.getAddress() + ",")
                    continue
                }
                Log.i("TAG", "$i " + name.name + " " + name.get(manhole).toString())
                dataList.add(name.get(manhole).toString() + ",")
            }
            dataList.add("-")
        }
        Log.i("TAG", dataList.toString())
        easyCsv.setSeparatorColumn(",")
        easyCsv.setSeperatorLine("-")
        val mainFile = File(this.getExternalFilesDir(null), "ManholeInspection")
        if (!mainFile.exists())
            mainFile.mkdirs()
        val mainFile2 = "ManholeInspection"
        val f = File(Environment.getExternalStorageDirectory(), mainFile2)
        if (!f.exists()) {
            f.mkdir()
        }
        val file = File("ManholeInspection/manholes_output")
        if (file.exists())
            file.delete()

        easyCsv.createCsvFile("ManholeInspection/manholes_output", headerList,
                dataList, permissionCode, object : FileCallback {
            override fun onSuccess(p0: File?) {
                Log.i("TAG", p0!!.absolutePath)
                runOnUiThread {
                    exportPipes()
                }
            }

            override fun onFail(p0: String?) {
                Log.i("TAG", "$p0 ")
                runOnUiThread {
                    showError()
                }
            }
        })
    }

    private fun exportPipes() {
        val easyCsv = EasyCsv(this)
        val headerList = ArrayList<String>()
        val dataList = ArrayList<String>()
        for (name in Pipe::class.memberProperties) {
            if (name.name !in Pipe.dic())
                continue
            Log.i("TAG", name.name)
            headerList.add(Pipe.dic()[name.name] + ",")
        }

        Log.i("TAG", "HeaderList size: " + headerList.size)
        headerList.add("-")
        for ((i, pipe) in pipes.withIndex()) {
            for (name in Pipe::class.memberProperties) {
                if (name.name !in Pipe.dic()) {
                    continue
                }
                if (name.name == "address") {
                    dataList.add(pipe.getAddress() + ",")
                    continue
                }
                Log.i("TAG", "$i " + name.name + " " + name.get(pipe).toString())
                dataList.add(name.get(pipe).toString() + ",")
            }
            dataList.add("-")
        }
        Log.i("TAG", dataList.toString())
        easyCsv.setSeparatorColumn(",")
        easyCsv.setSeperatorLine("-")
        val mainFile = File(this.getExternalFilesDir(null), "ManholeInspection")
        if (!mainFile.exists())
            mainFile.mkdirs()
        val mainFile2 = "ManholeInspection"
        val f = File(Environment.getExternalStorageDirectory(), mainFile2)
        if (!f.exists()) {
            f.mkdir()
        }
        val file = File("ManholeInspection/pipes_output")
        if (file.exists())
            file.delete()

        easyCsv.createCsvFile("ManholeInspection/pipes_output", headerList,
                dataList, permissionCode, object : FileCallback {
            override fun onSuccess(p0: File?) {
                Log.i("TAG", p0!!.absolutePath)
                runOnUiThread {
                    showSuccessfulDialog()
                }
            }

            override fun onFail(p0: String?) {
                Log.i("TAG", "$p0 ")
                runOnUiThread {
                    showError()
                }
            }
        })
    }

    private fun saveData() {
        GlobalScope.async {
            runOnUiThread {
                showWaitingDialog()
            }
            val repo = ManholeRepository(MyRoomDB.getDatabase(this@ProfileActivity).manholeDAO())
            val pipeRepo = PipeRepository(MyRoomDB.getDatabase(this@ProfileActivity).pipeDAO())
            export(repo.allManholes)
            pipes = pipeRepo.allPipes
        }
    }

    private fun showError() {
        if (sweetAlertDialog.isShowing)
            sweetAlertDialog.dismiss()
        sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        sweetAlertDialog.setTitle("مشکل!")
        sweetAlertDialog.contentText = "در ذخیره سازی اطلاعات مشکلی به وجود آمده است"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { sweetAlertDialog.dismiss() }
    }


    private fun showWaitingDialog() {
        if (sweetAlertDialog.isShowing)
            sweetAlertDialog.dismiss()
        sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        sweetAlertDialog.setTitle("لطفا شکیبا باشید")
        sweetAlertDialog.setCancelable(false)
        if (!isFinishing)
            sweetAlertDialog.show()
    }

    private fun showSuccessfulDialog() {
        if (sweetAlertDialog.isShowing)
            sweetAlertDialog.dismiss()
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
        sweetAlertDialog.setTitle("موفق!")
        sweetAlertDialog.contentText = "اطلاعات با موفقیت ذخیره شد"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { v -> sweetAlertDialog.dismiss() }
    }

    override fun onLocationChanged(p0: Location?) {
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }


}
