package com.idea.abfa

import android.content.Intent
import android.content.res.AssetManager
import android.graphics.Typeface
import android.os.Bundle
import android.view.Menu
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Manhole
import com.idea.abfa.repositories.ManholeRepository
import kotlinx.android.synthetic.main.activity_form2.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class Form2Activity : AppCompatActivity() {
    private var iranSans: Typeface? = null
    private var manhole: Manhole? = null

    private fun getLocation(): String {
        return when {
            radioSquare!!.isChecked -> radioSquare!!.text.toString()
            radioStreet!!.isChecked -> radioStreet!!.text.toString()
            radioSideWalk!!.isChecked -> radioSideWalk!!.text.toString()
            radioBoulevard!!.isChecked -> radioBoulevard!!.text.toString()
            radioChannel!!.isChecked -> radioChannel!!.text.toString()
            radioGreenSpace!!.isChecked -> radioGreenSpace!!.text.toString()
            else -> radioPrivateSpace!!.text.toString()
        }
    }

    private fun getTraffic(): String {
        return if (radioHeavy!!.isChecked) {
            radioHeavy!!.text.toString()
        } else {
            radioLight!!.text.toString()
        }
    }

    private fun getGateType(): String {
        return when {
            radioGateCastIron!!.isChecked -> radioGateCastIron!!.text.toString()
            radioComposite!!.isChecked -> radioComposite!!.text.toString()
            radioPolymer!!.isChecked -> radioPolymer!!.text.toString()
            else -> radioGateConcrete!!.text.toString()
        }
    }

    private fun getGateClass(): String {
        return if (radio2500!!.isChecked) {
            radio2500!!.text.toString()
        } else {
            "D4000"
        }
    }

    private fun getGateTypeClass(): String {
        return if (radioGateSimple!!.isChecked) {
            radioGateSimple!!.text.toString()
        } else {
            radioGateHinges!!.text.toString()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.history_more, menu)
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form2)
        val next = findViewById<Button>(R.id.next)
        val previous = findViewById<Button>(R.id.previous)
        manhole = intent.extras!!.get("manhole") as Manhole?

        diameter.setText(manhole?.diameter)
        when (manhole?.location) {
            radioSquare.text.toString() -> {
                radioSquare.isChecked = true
                radioStreet.isChecked = false
                radioSideWalk.isChecked = false
                radioBoulevard.isChecked = false
                radioChannel.isChecked = false
                radioGreenSpace.isChecked = false
                radioPrivateSpace.isChecked = false
            }
            radioStreet.text.toString() -> {
                radioSquare.isChecked = false
                radioStreet.isChecked = true
                radioSideWalk.isChecked = false
                radioBoulevard.isChecked = false
                radioChannel.isChecked = false
                radioGreenSpace.isChecked = false
                radioPrivateSpace.isChecked = false
            }
            radioSideWalk.text.toString() -> {
                radioSquare.isChecked = false
                radioStreet.isChecked = false
                radioSideWalk.isChecked = true
                radioBoulevard.isChecked = false
                radioChannel.isChecked = false
                radioGreenSpace.isChecked = false
                radioPrivateSpace.isChecked = false
            }
            radioBoulevard.text.toString() -> {
                radioSquare.isChecked = false
                radioStreet.isChecked = false
                radioSideWalk.isChecked = false
                radioBoulevard.isChecked = true
                radioChannel.isChecked = false
                radioGreenSpace.isChecked = false
                radioPrivateSpace.isChecked = false
            }
            radioChannel.text.toString() -> {
                radioSquare.isChecked = false
                radioStreet.isChecked = false
                radioSideWalk.isChecked = false
                radioBoulevard.isChecked = false
                radioChannel.isChecked = true
                radioGreenSpace.isChecked = false
                radioPrivateSpace.isChecked = false
            }
            radioGreenSpace.text.toString() -> {
                radioSquare.isChecked = false
                radioStreet.isChecked = false
                radioSideWalk.isChecked = false
                radioBoulevard.isChecked = false
                radioChannel.isChecked = false
                radioGreenSpace.isChecked = true
                radioPrivateSpace.isChecked = false
            }
            else -> {
                radioSquare.isChecked = false
                radioStreet.isChecked = false
                radioSideWalk.isChecked = false
                radioBoulevard.isChecked = false
                radioChannel.isChecked = false
                radioGreenSpace.isChecked = false
                radioPrivateSpace.isChecked = true
            }
        }

        when (manhole?.traffic) {
            radioHeavy.text.toString() -> {
                radioHeavy.isChecked = true
                radioLight.isChecked = false
            }
            else -> {
                radioHeavy.isChecked = false
                radioLight.isChecked = true
            }
        }

        when (manhole?.gateClass) {
            radio2500.text.toString() -> {
                radio2500.isChecked = true
                radio4000.isChecked = false
            }
            else -> {
                radio2500.isChecked = false
                radio4000.isChecked = true
            }
        }


        when (manhole?.gateType) {
            radioGateCastIron.text.toString() -> {
                radioGateCastIron.isChecked = true
                radioComposite.isChecked = false
                radioPolymer.isChecked = false
                radioGateConcrete.isChecked = false
            }
            radioComposite.text.toString() -> {
                radioGateCastIron.isChecked = false
                radioComposite.isChecked = true
                radioPolymer.isChecked = false
                radioGateConcrete.isChecked = false
            }
            radioPolymer.text.toString() -> {
                radioGateCastIron.isChecked = false
                radioComposite.isChecked = false
                radioPolymer.isChecked = true
                radioGateConcrete.isChecked = false
            }
            else -> {
                radioGateCastIron.isChecked = false
                radioComposite.isChecked = false
                radioPolymer.isChecked = false
                radioGateConcrete.isChecked = true
            }
        }

        when (manhole?.gateTypeClass) {
            radioGateSimple.text.toString() -> {
                radioGateSimple.isChecked = true
                radioGateHinges.isChecked = false
            }
            else -> {
                radioGateSimple.isChecked = false
                radioGateHinges.isChecked = true
            }
        }


        iranSans = getFont(assets, "fonts/iransans.ttf")
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        next.setOnClickListener {
            if (!checkFields()) {
                showErrorDialog()
                return@setOnClickListener
            }
            manhole?.diameter = diameter?.text.toString()
            manhole?.location = getLocation()
            manhole?.traffic = getTraffic()
            manhole?.gateClass = getGateClass()
            manhole?.gateType = getGateType()
            manhole?.gateTypeClass = getGateTypeClass()

            manhole?.let { it1 -> editManhole(it1) }
            val intent = Intent(this, Form3Activity::class.java)
            intent.putExtra("manhole", manhole)
            startActivity(intent)
        }

        previous.setOnClickListener {
            val intent = Intent(this, Form1Activity::class.java)
            intent.putExtra("manhole", manhole)
            startActivity(intent)
        }
    }

    private fun editManhole(manhole: Manhole) {
        GlobalScope.async {
            val repo = ManholeRepository(MyRoomDB.getDatabase(this@Form2Activity).manholeDAO())
            repo.update(manhole)
        }
    }

    private fun showErrorDialog() {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        sweetAlertDialog.setTitle("مشکل!")
        sweetAlertDialog.contentText = "همه بخش های لازم را پر کنید"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { sweetAlertDialog.dismiss() }
    }

    private fun checkFields(): Boolean {
        return true
    }

    private fun getFont(assetManager: AssetManager, path: String): Typeface {
        return Typeface.createFromAsset(assetManager, path)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
