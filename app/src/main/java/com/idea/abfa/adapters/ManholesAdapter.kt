package com.idea.abfa.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.idea.abfa.Form1Activity
import com.idea.abfa.PipesActivity
import com.idea.abfa.R
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Manhole
import com.idea.abfa.repositories.ManholeRepository
import kotlinx.android.synthetic.main.manhole_item.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class ManholesAdapter(var context: Context, private var manholesList: ArrayList<Manhole>)
    : RecyclerView.Adapter<ManholesAdapter.Item>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Item {
        val view = LayoutInflater.from(context).inflate(R.layout.manhole_item, parent, false)
        return Item(view)
    }

    override fun getItemCount() = manholesList.size

    override fun onBindViewHolder(holder: Item, position: Int) {
        val manhole = manholesList[position]
        holder.itemView.manhole_title.text = manhole.toString()
        holder.itemView.pipes.setOnClickListener {
            val intent = Intent(context, PipesActivity::class.java)
            intent.putExtra("manholeId", manhole.id)
            intent.putExtra("manhole", manhole)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
        }
        holder.itemView.deleteManhole.setOnClickListener {
            deleteItem(position)
        }
        holder.itemView.setOnClickListener {
            val intent = Intent(context, Form1Activity::class.java)
            intent.putExtra("manhole", manhole)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
        }
    }

    private fun deleteItem(position: Int) {
        deleteManhole(manholesList[position])
        manholesList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, manholesList.size)
        notifyDataSetChanged()
    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView)


    private fun deleteManhole(manhole: Manhole) {
        GlobalScope.async {
            val repo = ManholeRepository(MyRoomDB.getDatabase(context).manholeDAO())
            repo.delete(manhole)
            Log.i("TAG", "after del " + repo.allManholes.size.toString())
        }
    }
}