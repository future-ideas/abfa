package com.idea.abfa.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.idea.abfa.Form6Activity
import com.idea.abfa.R
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Pipe
import com.idea.abfa.repositories.PipeRepository
import kotlinx.android.synthetic.main.pipe_item.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class PipesAdapter(var context: Context, private var pipesList: ArrayList<Pipe>)
    : RecyclerView.Adapter<PipesAdapter.Item>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Item {
        val view = LayoutInflater.from(context).inflate(R.layout.pipe_item, parent, false)
        return Item(view)
    }

    override fun getItemCount() = pipesList.size

    override fun onBindViewHolder(holder: Item, position: Int) {
        val pipe = pipesList[position]
        holder.itemView.pipe_title.text = pipe.toString()
        holder.itemView.deletePipe.setOnClickListener {
            deleteItem(position)
        }
        holder.itemView.setOnClickListener {
            goto(pipe)
        }
    }


    private fun deleteItem(position: Int) {
        deletePipe(pipesList[position])
        pipesList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, pipesList.size)
        notifyDataSetChanged()
    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView)


    private fun deletePipe(pipe: Pipe) {
        GlobalScope.async {
            val repo = PipeRepository(MyRoomDB.getDatabase(context).pipeDAO())
            repo.delete(pipe)
            Log.i("TAG", repo.allPipes.size.toString())
        }
    }

    private fun goto(pipe: Pipe) {

        val intent = Intent(context, Form6Activity::class.java)
        intent.putExtra("pipe", pipe)
        GlobalScope.async {
            val repo = PipeRepository(MyRoomDB.getDatabase(context).pipeDAO())
            intent.putExtra("manhole", repo.getManhole(pipe.manholeId))
            Log.i("TAG", repo.allPipes.size.toString())
            context.startActivity(intent)
        }
    }
}