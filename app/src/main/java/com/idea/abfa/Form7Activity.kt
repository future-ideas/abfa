package com.idea.abfa

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.AssetManager
import android.graphics.Color
import android.graphics.Typeface
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.Menu
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Manhole
import com.idea.abfa.models.Pipe
import com.idea.abfa.repositories.PipeRepository
import ir.hamsaa.persiandatepicker.Listener
import ir.hamsaa.persiandatepicker.PersianDatePickerDialog
import ir.hamsaa.persiandatepicker.util.PersianCalendar
import kotlinx.android.synthetic.main.activity_form7.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.text.SimpleDateFormat
import java.util.*


class Form7Activity : AppCompatActivity(), LocationListener {
    private var longitude: Double? = 0.0
    private var latitude: Double? = 0.0
    private var altidue: Double? = 0.0
    private var TAG_CODE_PERMISSION_LOCATION: Int = 105
    private var manhole: Manhole? = null
    private var pipe: Pipe? = null
    private var iranSans: Typeface? = null

    override fun onLocationChanged(p0: Location?) {
        longitude = p0!!.longitude
        latitude = p0.latitude
        altidue = p0.altitude
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }


    private fun getFont(assetManager: AssetManager, path: String): Typeface {
        return Typeface.createFromAsset(assetManager, path)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form7)
        val next = findViewById<Button>(R.id.next)
        val previous = findViewById<Button>(R.id.previous)
        iranSans = getFont(assets, "fonts/iransans.ttf")
        manhole = intent.extras!!.get("manhole") as Manhole?
        pipe = intent.extras!!.get("pipe") as Pipe?
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        ActivityCompat.requestPermissions(this, arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        ), TAG_CODE_PERMISSION_LOCATION)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {

            val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 5000L, 10f, this)
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0f, this)
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, this)
        }

        te15.text = pipe?.exploitation_date
        ed16.setText(pipe?.description)

        next.setOnClickListener {
            if (!checkFields()) {
                showErrorDialog()
                return@setOnClickListener
            }
            pipe?.description = ed16.text.toString()
            pipe?.exploitation_date = te15.text.toString()
            pipe?.longitude = longitude.toString()
            pipe?.latitude = latitude.toString()
            pipe?.altitude = altidue.toString()
            pipe?.date = getDateAndTime()[0]
            pipe?.time = getDateAndTime()[1]
            pipe?.manholeCode = manhole?.manholeCode.toString()
            pipe?.username = getSharedPreferences("pref", Context.MODE_PRIVATE).getString("username", "").toString()
            pipe?.munRegion = manhole?.munRegion.toString()
            editPipe(pipe!!)
            showSuccessfulDialog()
        }


        previous.setOnClickListener {
            val intent = Intent(this, Form6Activity::class.java)
            intent.putExtra("manhole", manhole)
            intent.putExtra("pipe", pipe)
            startActivity(intent)
        }

        te15.setOnClickListener {
            showDatePicker()
        }
    }

    private fun getDateAndTime(): Array<String> {
        val cal = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("yyyy/MM/dd")
        val hour = cal.get(Calendar.HOUR_OF_DAY)
        var t = "$hour:"
        val min = cal.get(Calendar.MINUTE)
        t += if (min < 10) {
            if (min == 0) {
                "00"
            } else {
                "0$min"
            }
        } else {
            "$min"
        }
        return arrayOf(simpleDateFormat.format(cal.time), t)
    }

    private fun editPipe(pipe: Pipe) {
        GlobalScope.async {
            val repo = PipeRepository(MyRoomDB.getDatabase(this@Form7Activity).pipeDAO())
            repo.update(pipe)
        }
    }


    private fun showError() {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        sweetAlertDialog.setTitle("مشکل!")
        sweetAlertDialog.contentText = "در ذخیره سازی اطلاعات مشکلی به وجود آمده است"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { sweetAlertDialog.dismiss() }
    }

    private fun showSuccessfulDialog() {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
        sweetAlertDialog.setTitle("موفق!")
        sweetAlertDialog.contentText = "اطلاعات با موفقیت ذخیره شد"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener {
            sweetAlertDialog.dismiss()
            val intent = Intent(this, ProfileActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
    }

    private fun showDatePicker() {
        val initDate = PersianCalendar()
        initDate.setPersianDate(1398, 1, 1)
        val picker = PersianDatePickerDialog(this)
                .setPositiveButtonString("باشه")
                .setNegativeButton("بیخیال")
                .setMinYear(1393)
                .setMaxYear(1500)
                .setInitDate(initDate)
                .setActionTextColor(Color.GRAY)
                .setTypeFace(iranSans)
                .setTitleType(PersianDatePickerDialog.WEEKDAY_DAY_MONTH_YEAR)
                .setShowInBottomSheet(true)
                .setListener(object : Listener {
                    override fun onDateSelected(persianCalendar: PersianCalendar) {
                        te15.text = persianCalendar.persianYear.toString() + "/" +
                                persianCalendar.persianMonth + "/" + persianCalendar.persianDay
                    }

                    override fun onDismissed() {

                    }
                })

        picker.show()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.history_more, menu)
        return true
    }

    private fun showErrorDialog() {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        sweetAlertDialog.setTitle("مشکل!")
        sweetAlertDialog.contentText = "همه بخش های لازم را پر کنید"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { v -> sweetAlertDialog.dismiss() }
    }

    private fun checkFields(): Boolean {
        return true
    }

}
