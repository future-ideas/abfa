package com.idea.abfa

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Address
import com.idea.abfa.models.Manhole
import com.idea.abfa.repositories.ManholeRepository
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.text.SimpleDateFormat
import java.util.*


class Form1Activity : AppCompatActivity(), LocationListener {

    private val permissionCode: Int = 100
    private var checked: Boolean = false
    private var gpsChecked: Boolean = false
    private var status: Boolean = false
    private var TAG_CODE_PERMISSION_LOCATION: Int = 105
    private var edit: Boolean = false
    private var lon: Double? = 0.0
    private var lat: Double? = 0.0
    private var alt: Double? = 0.0
    private lateinit var manhole: Manhole

    private val crossType: String
        get() = if (radioCircle!!.isChecked) {
            radioCircle!!.text.toString()
        } else {
            radioFourSquare!!.text.toString()
        }

    private val specialCrossTypeRadio: String
        get() = when {
            radioSpecialCircle!!.isChecked -> radioSpecialCircle!!.text.toString()
            radioSpecialPolygon!!.isChecked -> radioSpecialPolygon!!.text.toString()
            else -> radioSpecialFourSquare!!.text.toString()
        }

    private val type: String
        get() = when {
            radioConcrete!!.isChecked -> radioConcrete!!.text.toString()
            radioBrickAndPlaster!!.isChecked -> radioBrickAndPlaster!!.text.toString()
            radioBrick!!.isChecked -> radioBrick!!.text.toString()
            radioPE!!.isChecked -> radioPE!!.text.toString()
            else -> radioGRP!!.text.toString()
        }


    private val radio: String
        get() = if (radioTypical!!.isChecked) {
            radioTypical.text.toString()
        } else {
            radioSpecial.text.toString()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)
        askPermissions()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityCompat.requestPermissions(this, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            ), TAG_CODE_PERMISSION_LOCATION)

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {

                Log.i("TAG", "Granted")
                val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 5000L, 10f, this)
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0f, this)
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, this)
                val location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                if (location != null) {
                    lat = location.latitude
                    alt = location.altitude
                    lon = location.longitude
                }
            }
        } else {
            val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 5000L, 10f, this)
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0f, this)
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, this)

            val location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (location != null) {
                lat = location.latitude
                alt = location.altitude
                lon = location.longitude
            }

        }
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        if (intent.extras != null && intent.extras!!.get("manhole") != null) {
            edit = true
            manhole = intent.extras!!.get("manhole") as Manhole
            name.setText(manhole.username)
            region.setText(manhole.munRegion)
            main_address.setText(manhole.address?.mainStreet)
            sub_address.setText(manhole.address?.subStreet)
            alley.setText(manhole.address?.alley)
            blind_alley.setText(manhole.address?.blindAlley)
            manhole_code.setText(manhole.manholeCode)

            latitude.setText(manhole.gateAltitude)
            longitude.setText(manhole.gateLongitude)
            altitude.setText(manhole.gateAltitude)

            depth.setText(manhole.depth)
            if (manhole.typeClass == radioTypical.text.toString()) {
                radioTypical.isChecked = true
                radioSpecial.isChecked = false
            } else {
                radioTypical.isChecked = false
                radioSpecial.isChecked = true
            }
            Log.i("TAG", manhole.typeClass + " " + manhole.type + " " + manhole.crossForm + " " + manhole.specialCrossType)

            if (manhole.crossForm == radioCircle.text.toString()) {
                radioCircle.isChecked = true
                radioFourSquare.isChecked = false
            } else {
                radioCircle.isChecked = false
                radioFourSquare.isChecked = true
            }

            when (manhole.specialCrossType) {
                radioSpecialCircle.text.toString() -> {
                    radioSpecialCircle.isChecked = true
                    radioSpecialFourSquare.isChecked = false
                    radioSpecialPolygon.isChecked = false
                }
                radioSpecialFourSquare.text.toString() -> {
                    radioSpecialCircle.isChecked = false
                    radioSpecialFourSquare.isChecked = true
                    radioSpecialPolygon.isChecked = false
                }
                else -> {
                    radioSpecialCircle.isChecked = false
                    radioSpecialFourSquare.isChecked = false
                    radioSpecialPolygon.isChecked = true
                }
            }

            when (manhole.type) {
                radioConcrete.text.toString() -> {
                    radioConcrete.isChecked = true
                    radioBrick.isChecked = false
                    radioBrickAndPlaster.isChecked = false
                    radioPE.isChecked = false
                    radioGRP.isChecked = false
                }
                radioBrick.text.toString() -> {
                    radioConcrete.isChecked = false
                    radioBrick.isChecked = true
                    radioBrickAndPlaster.isChecked = false
                    radioPE.isChecked = false
                    radioGRP.isChecked = false
                }
                radioBrickAndPlaster.text.toString() -> {
                    radioConcrete.isChecked = false
                    radioBrick.isChecked = false
                    radioBrickAndPlaster.isChecked = true
                    radioPE.isChecked = false
                    radioGRP.isChecked = false
                }
                radioPE.text.toString() -> {
                    radioConcrete.isChecked = false
                    radioBrick.isChecked = false
                    radioBrickAndPlaster.isChecked = false
                    radioPE.isChecked = true
                    radioGRP.isChecked = false
                }
                else -> {
                    radioConcrete.isChecked = false
                    radioBrick.isChecked = false
                    radioBrickAndPlaster.isChecked = false
                    radioPE.isChecked = false
                    radioGRP.isChecked = true
                }
            }


        }


        next.setOnClickListener {
            if (!checked && !checkFields()) {
                showErrorDialog()
                return@setOnClickListener
            }

            val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            if (!statusOfGPS && !gpsChecked) {
                showGPSErrorDialog()
                return@setOnClickListener
            }
            if (edit) {
                showUpdateDialog(manhole)
            } else {
                val manhole = getManhole()
                insertManhole(manhole)
            }
        }
    }


    private fun askPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED)) { // Should we show an explanation?
                shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_EXTERNAL_STORAGE) // Explain to the user why we need to read the contacts
                shouldShowRequestPermissionRationale(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) // Explain to the user why we need to read the contacts
                shouldShowRequestPermissionRationale(
                        Manifest.permission.CAMERA) // Explain to the user why we need to read the contacts
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA),
                        permissionCode)
                // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
// app-defined int constant that should be quite unique

            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (permissionCode == requestCode) {
            if (grantResults.isEmpty()) {
                askPermissions()
            }
        }
    }

    private fun gotoForm2(manhole: Manhole) {
        val intent = Intent(this, Form2Activity::class.java)
        intent.putExtra("manhole", manhole)
        Log.i("TAG", manhole.id.toString())
        startActivity(intent)
    }

    private fun insertManhole(manhole: Manhole) {
        GlobalScope.async {
            val repo = ManholeRepository(MyRoomDB.getDatabase(this@Form1Activity).manholeDAO())
            manhole.id = repo.insert(manhole).toInt()
            runOnUiThread {
                gotoForm2(manhole)
            }
        }
    }

    private fun editManhole(manhole: Manhole) {
        manhole.username = getSharedPreferences("pref", Context.MODE_PRIVATE).getString("username", "").toString()
        manhole.munRegion = region!!.text.toString()
        manhole.address = Address(main_address?.text.toString())
        manhole.address!!.subStreet = sub_address?.text.toString()
        manhole.address!!.alley = alley?.text.toString()
        manhole.manholeCode = manhole_code?.text.toString()
        manhole.address!!.blindAlley = blind_alley?.text.toString()
        manhole.type = type
        manhole.typeClass = radio
        manhole.crossForm = crossType
        manhole.specialCrossType = specialCrossTypeRadio
        manhole.gateLongitude = lon.toString()
        manhole.gateLatitude = lat.toString()
        manhole.gateAltitude = alt.toString()
        manhole.depth = depth.text.toString()

        Log.i("TAG", " " + manhole.type + " " + manhole.typeClass)
        Log.i("TAG", " " + manhole.gateLatitude + " " + manhole.gateLongitude + " " + manhole.gateAltitude)
        val arr = getDateAndTime()
        manhole.date = arr[0]
        manhole.time = arr[1]
        GlobalScope.async {
            val repo = ManholeRepository(MyRoomDB.getDatabase(this@Form1Activity).manholeDAO())
            Log.i("TAG", "Manhole id: " + manhole.id)
            repo.update(manhole)
            runOnUiThread {
                gotoForm2(manhole)
            }
        }
    }

    private fun getDateAndTime(): Array<String> {
        val cal = Calendar.getInstance()
        val simpleDateFormat = SimpleDateFormat("yyyy/MM/dd")
        val hour = cal.get(Calendar.HOUR_OF_DAY)
        var t = "$hour:"
        val min = cal.get(Calendar.MINUTE)
        t += if (min < 10) {
            if (min == 0) {
                "00"
            } else {
                "0$min"
            }
        } else {
            "$min"
        }
        return arrayOf(simpleDateFormat.format(cal.time), t)
    }

    private fun getManhole(): Manhole {
        val manhole = Manhole()
        manhole.username = name?.text.toString()
        manhole.munRegion = region!!.text.toString()
        manhole.address = Address(main_address?.text.toString())
        manhole.address!!.subStreet = sub_address?.text.toString()
        manhole.address!!.alley = alley?.text.toString()
        manhole.manholeCode = manhole_code?.text.toString()
        manhole.address!!.blindAlley = blind_alley?.text.toString()
        manhole.type = type
        manhole.typeClass = radio
        manhole.crossForm = crossType
        manhole.specialCrossType = specialCrossTypeRadio
        manhole.gateLongitude = lon.toString()
        manhole.gateLatitude = lat.toString()
        manhole.gateAltitude = alt.toString()
        manhole.depth = depth.text.toString()
        manhole.date = getDateAndTime()[0]
        manhole.time = getDateAndTime()[1]
        return manhole
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.history_more, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection

        return super.onOptionsItemSelected(item)
    }

    private fun history() {
        val intent = Intent(this, HistoryActivity::class.java)
        startActivity(intent)
    }

    private fun checkFields(): Boolean {
        if (status)
            return true
        return name!!.text.toString().isNotEmpty() && region!!.text.toString().isNotEmpty() &&
                main_address!!.text.toString().isNotEmpty()
    }

    private fun showErrorDialog() {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        sweetAlertDialog.setTitle("مشکل!")
        sweetAlertDialog.contentText = "همه بخش های لازم را پر کنید"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { checked = true; sweetAlertDialog.dismiss() }
    }

    private fun showGPSErrorDialog() {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        sweetAlertDialog.setTitle("مشکل!")
        sweetAlertDialog.contentText = "اتصال به GPS خود را وارسی کنید"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { gpsChecked = true; sweetAlertDialog.dismiss() }
    }

    private fun showUpdateDialog(manhole: Manhole) {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
        sweetAlertDialog.setTitle("اخطار!")
        sweetAlertDialog.contentText = " اطلاعات در صورت تغییر جایگزین اطلاعات قبلی می شوند"
        if (!isFinishing)
            sweetAlertDialog.show()
        sweetAlertDialog.setOnCancelListener { sweetAlertDialog.dismiss() }
        sweetAlertDialog.setConfirmClickListener {
            sweetAlertDialog.dismiss()
            editManhole(manhole)
        }
    }

    override fun onLocationChanged(p0: Location?) {
        if (p0 != null) {
            lon = p0.longitude
            lat = p0.latitude
            alt = p0.altitude
        }
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
