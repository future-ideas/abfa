package com.idea.abfa;

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Manhole
import com.idea.abfa.repositories.ManholeRepository
import kotlinx.android.synthetic.main.activity_form4.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class Form4Activity : AppCompatActivity() {

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private var manhole: Manhole? = null

    private fun getStairsStatus(): String {
        return if (radioNormal!!.isChecked) {
            radioNormal!!.text.toString()
        } else {
            radioDefective!!.text.toString()
        }
    }

    private fun getDrop(): String {
        return if (radioDropYes!!.isChecked) {
            radioDropYes!!.text.toString()
        } else {
            radioDropNo!!.text.toString()
        }
    }

    private fun getBroken(): String {
        return if (radioBrokenYes!!.isChecked) {
            radioBrokenYes!!.text.toString()
        } else {
            radioBrokenNo!!.text.toString()
        }
    }

    private fun getHoles(): String {
        return if (radioHoleYes!!.isChecked) {
            radioHoleYes!!.text.toString()
        } else {
            radioHoleNo!!.text.toString()
        }
    }

    private fun getMuscleStatus(): String {
        return when {
            radioMuscleNormal!!.isChecked -> {
                radioMuscleNormal.text.toString()
            }
            radioMuscleDemolished!!.isChecked -> {
                radioMuscleDemolished.text.toString()
            }
            else -> {
                radioMuscleDemolishing.text.toString()
            }
        }
    }

    private fun getCanalStatus(): String {
        return when {
            radioCanalNormal!!.isChecked -> {
                radioCanalNormal!!.text.toString()
            }
            radioCanalDefective!!.isChecked -> {
                radioCanalDefective!!.text.toString()
            }
            else -> {
                radioCanalInvisible!!.text.toString()
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form4)
        val next = findViewById<Button>(R.id.next)
        val previous = findViewById<Button>(R.id.previous)
        manhole = intent.extras!!.get("manhole") as Manhole?
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)


        when (manhole?.drop) {
            radioDropYes.text!!.toString() -> {
                radioDropYes!!.isChecked = true
                radioDropNo.isChecked = false
            }
            else -> {
                radioDropYes!!.isChecked = false
                radioDropNo.isChecked = true
            }
        }

        when (manhole?.broken) {
            radioBrokenYes.text!!.toString() -> {
                radioBrokenYes!!.isChecked = true
                radioBrokenNo.isChecked = false
            }
            else -> {
                radioBrokenYes!!.isChecked = false
                radioBrokenNo.isChecked = true
            }
        }
        when (manhole?.canalStatus) {
            radioCanalNormal.text!!.toString() -> {
                radioCanalNormal!!.isChecked = true
                radioCanalDefective.isChecked = false
                radioCanalInvisible.isChecked = false
            }
            radioCanalDefective.text!!.toString() -> {
                radioCanalNormal!!.isChecked = false
                radioCanalDefective.isChecked = true
                radioCanalInvisible.isChecked = false
            }
            else -> {
                radioCanalNormal!!.isChecked = false
                radioCanalDefective.isChecked = false
                radioCanalInvisible.isChecked = true
            }
        }


        when (manhole?.holes) {
            radioHoleYes.text!!.toString() -> {
                radioHoleYes!!.isChecked = true
                radioHoleNo.isChecked = false
            }
            else -> {
                radioHoleYes!!.isChecked = false
                radioHoleNo.isChecked = true
            }
        }

        when (manhole?.muscleStatus) {
            radioMuscleNormal.text!!.toString() -> {
                radioMuscleNormal!!.isChecked = true
                radioMuscleDemolished.isChecked = false
                radioMuscleDemolishing.isChecked = false
            }
            radioMuscleDemolished.text!!.toString() -> {
                radioMuscleNormal!!.isChecked = false
                radioMuscleDemolished.isChecked = true
                radioMuscleDemolishing.isChecked = false
            }
            else -> {
                radioMuscleNormal!!.isChecked = false
                radioMuscleDemolished.isChecked = false
                radioMuscleDemolishing.isChecked = true
            }
        }

        when (manhole?.stairsStatus) {
            radioNormal.text!!.toString() -> {
                radioNormal!!.isChecked = true
                radioDefective.isChecked = false
            }
            else -> {
                radioNormal!!.isChecked = false
                radioDefective.isChecked = true
            }
        }

        faulty_numbers?.setText(manhole?.faultyNumbers)
        normal_branches_number?.setText(manhole?.normalBranchesNumber)
        non_regular_branches_number?.setText(manhole?.nonRegularBranchesNumber)
        sewer_depth.setText(manhole?.sewerDepth)
        sewer_lines_number?.setText(manhole?.sewerLinesNumber)

        next.setOnClickListener {
            if (!checkFields()) {
                showErrorDialog()
                return@setOnClickListener
            }
            manhole?.drop = getDrop()
            manhole?.broken = getBroken()
            manhole?.canalStatus = getCanalStatus()
            manhole?.holes = getHoles()
            manhole?.muscleStatus = getMuscleStatus()
            manhole?.stairsStatus = getStairsStatus()
            manhole?.faultyNumbers = faulty_numbers?.text.toString()
            manhole?.normalBranchesNumber = normal_branches_number?.text.toString()
            manhole?.nonRegularBranchesNumber = non_regular_branches_number?.text.toString()
            manhole?.sewerLinesNumber = sewer_lines_number?.text.toString()
            manhole?.sewerDepth = sewer_depth?.text.toString()
            manhole?.let { it1 -> editManhole(it1) }
            val intent = Intent(this, Form5Activity::class.java)
            intent.putExtra("manhole", manhole)
            startActivity(intent)
        }

        previous.setOnClickListener {
            val intent = Intent(this, Form3Activity::class.java)
            intent.putExtra("manhole", manhole)
            startActivity(intent)
        }

    }

    private fun editManhole(manhole: Manhole) {
        GlobalScope.async {
            val repo = ManholeRepository(MyRoomDB.getDatabase(this@Form4Activity).manholeDAO())
            repo.update(manhole)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.history_more, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection

        return super.onOptionsItemSelected(item)
    }

    private fun history() {
        val intent = Intent(this, HistoryActivity::class.java)
        startActivity(intent)
    }


    private fun showErrorDialog() {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        sweetAlertDialog.setTitle("مشکل!")
        sweetAlertDialog.contentText = "همه بخش های لازم را پر کنید"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { v -> sweetAlertDialog.dismiss() }
    }

    private fun checkFields(): Boolean {
        return true
    }

}
