package com.idea.abfa.repositories

import com.idea.abfa.db.ManholeDAO
import com.idea.abfa.models.Manhole

class ManholeRepository(private val manholeDAO: ManholeDAO) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allManholes: List<Manhole> = manholeDAO.getManholes()

    suspend fun insert(manhole: Manhole): Long {
        return manholeDAO.insert(manhole)
    }

    suspend fun update(manhole: Manhole) {
        manholeDAO.update(manhole)
    }

    suspend fun delete(manhole: Manhole) {
        manholeDAO.delete(manhole)
    }
}