package com.idea.abfa.repositories

import com.idea.abfa.db.PipeDAO
import com.idea.abfa.models.Manhole
import com.idea.abfa.models.Pipe

class PipeRepository(private val pipeDAO: PipeDAO) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allPipes: List<Pipe> = pipeDAO.getPipes()

    suspend fun insert(pipe: Pipe): Long {
        return pipeDAO.insert(pipe)
    }

    suspend fun update(pipe: Pipe) {
        pipeDAO.update(pipe)
    }

    suspend fun delete(pipe: Pipe) {
        pipeDAO.delete(pipe)
    }

    fun getManhole(manholeId: Int): Manhole {
        return pipeDAO.getManhole(manholeId)
    }
}