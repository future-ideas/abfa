package com.idea.abfa.repositories

import androidx.lifecycle.LiveData
import com.idea.abfa.db.UserDAO
import com.idea.abfa.models.User

class UserRepository(private val userDAO: UserDAO) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allUsers: LiveData<List<User>> = userDAO.getUsers()

    suspend fun insert(user: User) {
        userDAO.insert(user)
    }
}