package com.idea.abfa

import android.content.Intent
import android.content.res.AssetManager
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Manhole
import com.idea.abfa.models.Pipe
import com.idea.abfa.repositories.PipeRepository
import ir.hamsaa.persiandatepicker.Listener
import ir.hamsaa.persiandatepicker.PersianDatePickerDialog
import ir.hamsaa.persiandatepicker.util.PersianCalendar
import kotlinx.android.synthetic.main.activity_form6.*
import kotlinx.coroutines.*

class Form6Activity : AppCompatActivity() {

    private var manhole: Manhole? = null
    private lateinit var pipe: Pipe
    private var iranSans: Typeface? = null
    private var edit = false

    private fun getFont(assetManager: AssetManager, path: String): Typeface {
        return Typeface.createFromAsset(assetManager, path)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form6)
        val next = findViewById<Button>(R.id.next)
        val previous = findViewById<Button>(R.id.previous)
        iranSans = getFont(assets, "fonts/iransans.ttf")
        manhole = intent.extras!!.get("manhole") as Manhole?

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        te14.setOnClickListener {
            showDatePicker()
        }


        if (intent.extras != null && intent.extras!!.get("pipe") != null) {
            edit = true
            pipe = intent.extras!!.get("pipe") as Pipe
            ed1.setText(pipe.pipe_code)
            ed2.setText(pipe.upper_manhole_code)
            ed3.setText(pipe.lower_manhole_code)
            ed4.setText(pipe.pipe_diameter)
            ed5.setText(pipe.first_depth)
            ed6.setText(pipe.last_depth)

            when (pipe.type) {
                ra7_1.text.toString() -> {
                    ra7_1.isChecked = true
                    ra7_2.isChecked = false
                    ra7_3.isChecked = false
                    ra7_4.isChecked = false
                    ra7_5.isChecked = false
                    ra7_6.isChecked = false
                    ra7_7.isChecked = false
                    ra7_8.isChecked = false
                }

                ra7_2.text.toString() -> {
                    ra7_1.isChecked = false
                    ra7_2.isChecked = true
                    ra7_3.isChecked = false
                    ra7_4.isChecked = false
                    ra7_5.isChecked = false
                    ra7_6.isChecked = false
                    ra7_7.isChecked = false
                    ra7_8.isChecked = false
                }

                ra7_3.text.toString() -> {
                    ra7_1.isChecked = false
                    ra7_2.isChecked = false
                    ra7_3.isChecked = true
                    ra7_4.isChecked = false
                    ra7_5.isChecked = false
                    ra7_6.isChecked = false
                    ra7_7.isChecked = false
                    ra7_8.isChecked = false
                }

                ra7_4.text.toString() -> {
                    ra7_1.isChecked = false
                    ra7_2.isChecked = false
                    ra7_3.isChecked = false
                    ra7_4.isChecked = true
                    ra7_5.isChecked = false
                    ra7_6.isChecked = false
                    ra7_7.isChecked = false
                    ra7_8.isChecked = false
                }

                ra7_5.text.toString() -> {
                    ra7_1.isChecked = false
                    ra7_2.isChecked = false
                    ra7_3.isChecked = false
                    ra7_4.isChecked = false
                    ra7_5.isChecked = true
                    ra7_6.isChecked = false
                    ra7_7.isChecked = false
                    ra7_8.isChecked = false
                }

                ra7_6.text.toString() -> {
                    ra7_1.isChecked = false
                    ra7_2.isChecked = false
                    ra7_3.isChecked = false
                    ra7_4.isChecked = false
                    ra7_5.isChecked = false
                    ra7_6.isChecked = true
                    ra7_7.isChecked = false
                    ra7_8.isChecked = false
                }

                ra7_7.text.toString() -> {
                    ra7_1.isChecked = false
                    ra7_2.isChecked = false
                    ra7_3.isChecked = false
                    ra7_4.isChecked = false
                    ra7_5.isChecked = false
                    ra7_6.isChecked = false
                    ra7_7.isChecked = true
                    ra7_8.isChecked = false
                }

                ra7_8.text.toString() -> {
                    ra7_1.isChecked = false
                    ra7_2.isChecked = false
                    ra7_3.isChecked = false
                    ra7_4.isChecked = false
                    ra7_5.isChecked = false
                    ra7_6.isChecked = false
                    ra7_7.isChecked = false
                    ra7_8.isChecked = true
                }
            }
            when (pipe.cross_form) {
                ra8_1.text.toString() -> {
                    ra8_1.isChecked = true
                    ra8_3.isChecked = false
                    ra8_4.isChecked = false
                    ra8_5.isChecked = false
                    ra8_6.isChecked = false
                    ra8_7.isChecked = false
                }

                ra8_3.text.toString() -> {
                    ra8_1.isChecked = false
                    ra8_3.isChecked = true
                    ra8_4.isChecked = false
                    ra8_5.isChecked = false
                    ra8_6.isChecked = false
                    ra8_7.isChecked = false
                }

                ra8_4.text.toString() -> {
                    ra8_1.isChecked = false
                    ra8_3.isChecked = false
                    ra8_4.isChecked = true
                    ra8_5.isChecked = false
                    ra8_6.isChecked = false
                    ra8_7.isChecked = false
                }

                ra8_5.text.toString() -> {
                    ra8_1.isChecked = false
                    ra8_3.isChecked = false
                    ra8_4.isChecked = false
                    ra8_5.isChecked = true
                    ra8_6.isChecked = false
                    ra8_7.isChecked = false
                }

                ra8_6.text.toString() -> {
                    ra8_1.isChecked = false
                    ra8_3.isChecked = false
                    ra8_4.isChecked = false
                    ra8_5.isChecked = false
                    ra8_6.isChecked = true
                    ra8_7.isChecked = false
                }

                ra8_7.text.toString() -> {
                    ra8_1.isChecked = false
                    ra8_3.isChecked = false
                    ra8_4.isChecked = false
                    ra8_5.isChecked = false
                    ra8_6.isChecked = false
                    ra8_7.isChecked = true
                }
            }

            when (pipe.location) {
                ra9_1.text.toString() -> {
                    ra9_1.isChecked = true
                    ra9_2.isChecked = false
                    ra9_3.isChecked = false
                    ra9_4.isChecked = false
                    ra9_5.isChecked = false
                    ra9_6.isChecked = false
                    ra9_7.isChecked = false
                }

                ra9_2.text.toString() -> {
                    ra9_1.isChecked = false
                    ra9_2.isChecked = true
                    ra9_3.isChecked = false
                    ra9_4.isChecked = false
                    ra9_5.isChecked = false
                    ra9_6.isChecked = false
                    ra9_7.isChecked = false
                }

                ra9_3.text.toString() -> {
                    ra9_1.isChecked = false
                    ra9_2.isChecked = false
                    ra9_3.isChecked = true
                    ra9_4.isChecked = false
                    ra9_5.isChecked = false
                    ra9_6.isChecked = false
                    ra9_7.isChecked = false
                }

                ra9_4.text.toString() -> {
                    ra9_1.isChecked = false
                    ra9_2.isChecked = false
                    ra9_3.isChecked = false
                    ra9_4.isChecked = true
                    ra9_5.isChecked = false
                    ra9_6.isChecked = false
                    ra9_7.isChecked = false
                }

                ra9_5.text.toString() -> {
                    ra9_1.isChecked = false
                    ra9_2.isChecked = false
                    ra9_3.isChecked = false
                    ra9_4.isChecked = false
                    ra9_5.isChecked = true
                    ra9_6.isChecked = false
                    ra9_7.isChecked = false
                }

                ra9_6.text.toString() -> {
                    ra9_1.isChecked = false
                    ra9_2.isChecked = false
                    ra9_3.isChecked = false
                    ra9_4.isChecked = false
                    ra9_5.isChecked = false
                    ra9_6.isChecked = true
                    ra9_7.isChecked = false
                }

                ra9_7.text.toString() -> {
                    ra9_1.isChecked = false
                    ra9_2.isChecked = false
                    ra9_3.isChecked = false
                    ra9_4.isChecked = false
                    ra9_5.isChecked = false
                    ra9_6.isChecked = false
                    ra9_7.isChecked = true
                }
            }
            when (pipe.traffic) {
                ra11_1.text.toString() -> {
                    ra11_1.isChecked = true
                    ra11_2.isChecked = false
                    ra11_3.isChecked = false
                }

                ra11_2.text.toString() -> {
                    ra11_1.isChecked = false
                    ra11_2.isChecked = true
                    ra11_3.isChecked = false
                }

                ra11_3.text.toString() -> {
                    ra11_1.isChecked = false
                    ra11_2.isChecked = false
                    ra11_3.isChecked = true
                }
            }
            ed10.setText(pipe.pipe_length)
            ed12.setText(pipe.slope)
            ed13.setText(pipe.profile_code)
            te14.text = pipe.execute_date
            pipe.manholeCode = manhole?.manholeCode.toString()
            pipe.address = manhole?.address
        }

        next.setOnClickListener {
            if (!checkFields()) {
                showErrorDialog()
                return@setOnClickListener
            }

            if (edit) {
                showUpdateDialog(pipe)
            } else {
                pipe = Pipe()
                pipe.address = manhole?.address
                pipe.manholeCode = manhole?.manholeCode.toString()
                pipe.pipe_code = ed1.text.toString()
                pipe.upper_manhole_code = ed2.text.toString()
                pipe.lower_manhole_code = ed3.text.toString()
                pipe.pipe_diameter = ed4.text.toString()
                pipe.first_depth = ed5.text.toString()
                pipe.last_depth = ed6.text.toString()
                pipe.type = getPipeType()
                pipe.cross_form = getSewageCross()
                pipe.location = getSewageLocation()
                pipe.pipe_length = ed10.text.toString()
                pipe.traffic = getTraffic()
                pipe.slope = ed12.text.toString()
                pipe.profile_code = ed13.text.toString()
                pipe.execute_date = te14.text.toString()
                pipe.manholeId = manhole!!.id
                Log.i("TAG", pipe?.manholeId.toString())
                runBlocking {
                    withContext(Dispatchers.IO) {
                        insertPipe(pipe!!)
                    }
                }
            }
        }

        previous.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
    }


    private fun showUpdateDialog(pipe: Pipe) {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
        sweetAlertDialog.setTitle("اخطار!")
        sweetAlertDialog.contentText = " اطلاعات در صورت تغییر جایگزین اطلاعات قبلی می شوند"
        if (!isFinishing)
            sweetAlertDialog.show()
        sweetAlertDialog.setOnCancelListener { sweetAlertDialog.dismiss() }
        sweetAlertDialog.setConfirmClickListener {
            sweetAlertDialog.dismiss()
            editPipe(pipe)
        }
    }

    private suspend fun insertPipe(pipe: Pipe) {
        val repo = PipeRepository(MyRoomDB.getDatabase(this@Form6Activity).pipeDAO())
        pipe.pipeId = repo.insert(pipe).toInt()
        Log.i("TAG", " " + pipe.pipeId + " " + pipe.manholeId)
        runOnUiThread {
            gotoForm7(pipe, manhole!!)
        }
    }

    private fun gotoForm7(pipe: Pipe, manhole: Manhole) {
        val intent = Intent(this, Form7Activity::class.java)
        intent.putExtra("manhole", manhole)
        intent.putExtra("pipe", pipe)
        startActivity(intent)
    }

    private fun editPipe(pipe: Pipe) {
        GlobalScope.async {
            val repo = PipeRepository(MyRoomDB.getDatabase(this@Form6Activity).pipeDAO())
            repo.update(pipe)
            runOnUiThread {
                gotoForm7(pipe, manhole!!)
            }
        }
    }

    private fun showDatePicker() {
        val initDate = PersianCalendar()
        initDate.setPersianDate(1398, 1, 1)
        val picker = PersianDatePickerDialog(this)
                .setPositiveButtonString("باشه")
                .setNegativeButton("بیخیال")
                .setTodayButton("امروز")
                .setTodayButtonVisible(false)
                .setMinYear(1393)
                .setMaxYear(1500)
                .setInitDate(initDate)
                .setActionTextColor(Color.GRAY)
                .setTypeFace(iranSans)
                .setTitleType(PersianDatePickerDialog.WEEKDAY_DAY_MONTH_YEAR)
                .setShowInBottomSheet(true)
                .setListener(object : Listener {
                    override fun onDateSelected(persianCalendar: PersianCalendar) {
                        te14.text = persianCalendar.persianYear.toString() + "/" +
                                persianCalendar.persianMonth + "/" + persianCalendar.persianDay
                    }

                    override fun onDismissed() {

                    }
                })

        picker.show()
    }


    private fun getTraffic(): String {
        return when {
            ra11_1.isChecked -> ra11_1.text.toString()
            ra11_2.isChecked -> ra11_2.text.toString()
            else -> ra11_3.text.toString()
        }
    }


    private fun getSewageLocation(): String {
        return when {
            ra9_1.isChecked -> ra9_1.text.toString()
            ra9_3.isChecked -> ra9_3.text.toString()
            ra9_4.isChecked -> ra9_4.text.toString()
            ra9_5.isChecked -> ra9_5.text.toString()
            ra9_6.isChecked -> ra9_6.text.toString()
            else -> ra9_7.text.toString()
        }
    }


    private fun getSewageCross(): String {
        return when {
            ra8_1.isChecked -> ra8_1.text.toString()
            ra8_3.isChecked -> ra8_3.text.toString()
            ra8_4.isChecked -> ra8_4.text.toString()
            ra8_5.isChecked -> ra8_5.text.toString()
            ra8_6.isChecked -> ra8_6.text.toString()
            ra8_7.isChecked -> ra8_7.text.toString()
            ra8_7.isChecked -> ra8_9.text.toString()
            else -> ra8_10.text.toString()
        }
    }

    private fun getPipeType(): String {
        return when {
            ra7_1.isChecked -> ra7_1.text.toString()
            ra7_2.isChecked -> ra7_2.text.toString()
            ra7_3.isChecked -> ra7_3.text.toString()
            ra7_4.isChecked -> ra7_4.text.toString()
            ra7_5.isChecked -> ra7_5.text.toString()
            ra7_6.isChecked -> ra7_6.text.toString()
            ra7_7.isChecked -> ra7_7.text.toString()
            else -> ra7_8.text.toString()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.history_more, menu)
        return true
    }

    private fun showErrorDialog() {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        sweetAlertDialog.setTitle("مشکل!")
        sweetAlertDialog.contentText = "همه بخش های لازم را پر کنید"
        if (!isFinishing)
            sweetAlertDialog.show()

        sweetAlertDialog.setConfirmClickListener { sweetAlertDialog.dismiss() }
    }

    private fun checkFields(): Boolean {
        return true
    }
}
