package com.idea.abfa

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.idea.abfa.adapters.PipesAdapter
import com.idea.abfa.db.MyRoomDB
import com.idea.abfa.models.Manhole
import kotlinx.android.synthetic.main.activity_pipes.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PipesActivity : AppCompatActivity() {

    private lateinit var manhole: Manhole

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pipes)
        val manholeId = intent.extras?.getInt("manholeId")
        manhole = intent.extras?.get("manhole") as Manhole
        GlobalScope.launch {
            val xx = MyRoomDB.getDatabase(this@PipesActivity).manholeDAO()
            val pipes = xx.getPipes(manholeId!!) as ArrayList
            Log.i("TAG", "${pipes.size}")
            runOnUiThread {
                val adapter = PipesAdapter(this@PipesActivity, pipes)
                pipes_recycler.adapter = adapter
                pipes_recycler.layoutManager = LinearLayoutManager(this@PipesActivity,
                        LinearLayoutManager.VERTICAL, false)
            }
        }

        fab.setOnClickListener {
            val intent = Intent(this, Form6Activity::class.java)
            intent.putExtra("manhole", manhole)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
