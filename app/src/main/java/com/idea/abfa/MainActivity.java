package com.idea.abfa;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.idea.abfa.models.User;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {

    EditText username, password;
    Button submit;
    SweetAlertDialog dialog;
    List<User> users = new ArrayList<>();
    private int permissionCode = 105;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        submit = findViewById(R.id.submitBtn);
        new ReadJson().execute();

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (username.getText().toString().length() != 0 && password.getText()
                        .toString().length() != 0) {
                    submit.setBackground(getResources().getDrawable(R.drawable.enable_back_button));
                } else {
                    submit.setBackground(getResources().getDrawable(R.drawable.disable_back_button));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (username.getText().toString().length() != 0 && password.getText()
                        .toString().length() != 0) {
                    submit.setBackground(getResources().getDrawable(R.drawable.enable_back_button));
                } else {
                    submit.setBackground(getResources().getDrawable(R.drawable.disable_back_button));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        submit.setOnClickListener(view -> {
            String username_txt = username.getText().toString();
            String password_txt = password.getText().toString();
            Integer id = Auth(username_txt, password_txt);
            if (id != null) {
                Intent intent = new Intent(this, ProfileActivity.class);
                intent.putExtra("username", username_txt);
                intent.putExtra("id", String.valueOf(id));
                putName(username_txt);
                if (username_txt.equals("AMK")) {
                    putAdmin();
                } else {
                    popAdmin();
                }
                startActivity(intent);
                finish();
            } else {
                showErrorDialog();
            }
        });
    }

    private void putAdmin() {
        SharedPreferences shared = getSharedPreferences("pref", Context.MODE_PRIVATE);
        shared.edit().putBoolean("admin", true).commit();
    }

    private void putName(String name) {
        SharedPreferences shared = getSharedPreferences("pref", Context.MODE_PRIVATE);
        shared.edit().putString("username", name).commit();
    }


    private void popAdmin() {
        SharedPreferences shared = getSharedPreferences("pref", Context.MODE_PRIVATE);
        shared.edit().putBoolean("admin", false).commit();
    }


    public void askPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        permissionCode);
                // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
                // app-defined int constant that should be quite unique
            }
        }
    }


    public Integer Auth(String username, String pass) {
        User target = new User(username, pass);
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).equals(target))
                return users.get(i).getUserId();
        }
        return null;
    }

    private void showWaitingDialog() {
        dialog = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE);
        dialog.setTitle("لطفا صبر کنید");
        dialog.setCancelable(false);
        if (!MainActivity.this.isFinishing()) {
            dialog.show();
        }
    }

    private void dismissWaitingDialog() {
        if (!MainActivity.this.isFinishing()) {
            dialog.dismiss();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class ReadJson extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showWaitingDialog();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return getAssetJsonData();
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            users = parseJSON(json);
            dismissWaitingDialog();
        }
    }

    private List<User> parseJSON(String jsonString) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<User>>() {
        }.getType();
        return gson.fromJson(jsonString, type);
    }

    public String getAssetJsonData() {
        String json;
        try {
            InputStream is = getAssets().open("user_pass.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }


    private void showErrorDialog() {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitle("مشکل!");
        sweetAlertDialog.setContentText("نام کاربری یا رمز عبور خود را وارسی کنید");
        if (!isFinishing())
            sweetAlertDialog.show();

        sweetAlertDialog.setConfirmClickListener(v -> sweetAlertDialog.dismiss());
    }
}
