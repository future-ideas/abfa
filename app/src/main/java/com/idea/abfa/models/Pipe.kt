package com.idea.abfa.models

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import java.io.Serializable

@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
@Entity(tableName = "pipes_table", foreignKeys = [ForeignKey(entity = Manhole::class,
        parentColumns = arrayOf("id"), childColumns = arrayOf("manhole_id"), onDelete = CASCADE)])
data class Pipe(
        var pipe_code: String = "",
        var upper_manhole_code: String = "",
        var lower_manhole_code: String = "",
        var pipe_diameter: String = "",
        var first_depth: String = "",
        var last_depth: String = "",
        var type: String = "",
        var cross_form: String = "",
        var execute_date: String = "تاریخ اجرا",
        var exploitation_date: String = "تاريخ  آغاز بهره برداري",
        var pipe_length: String = "",
        var traffic: String = "",
        var slope: String = "",
        var profile_code: String = "",
        var description: String = "",
        var manholeCode: String = "",
        var longitude: String = "",
        var latitude: String = "",
        var altitude: String = "",
        var location: String = "") : Serializable {

    @ColumnInfo(name = "username")
    var username: String = ""

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var pipeId: Int = 0

    @ColumnInfo(name = "manhole_id", index = true)
    var manholeId: Int = 0

    @ColumnInfo(name = "mun_region")
    var munRegion: String = ""
    var date: String = ""
    var time: String = ""
    @Embedded
    var address: Address? = null

    override fun toString(): String {
        return "لوله با کد $pipe_code در $location"
    }


    fun getAddress(): String {
        return address?.mainStreet + "/" + address?.subStreet + "/" + address?.alley + "/" + address?.blindAlley
    }


    companion object {
        fun dic(): HashMap<String, String> {
            val dic = HashMap<String, String>()
            dic["pipe_code"] = "کد لوله"
            dic["upper_manhole_code"] = "کد منهول بالا دست"
            dic["lower_manhole_code"] = "کد منهول پایین دست"
            dic["pipe_diameter"] = "قطر لوله"
            dic["first_depth"] = "عمق کف لوله در ابتدا"
            dic["last_depth"] = "عمق کف لوله در انتها"
            dic["type"] = "جنس لوله"
            dic["cross_form"] = "شکل سطح مقطع فاضلابرو"
            dic["execute_date"] = "تاریخ اجرا"
            dic["exploitation_date"] = "تاريخ  آغاز بهره برداري"
            dic["pipe_length"] = "طول لوله کارشده بین دو منهول"
            dic["traffic"] = "بار ترافیکی "
            dic["slope"] = "شیب لوله"
            dic["profile_code"] = "کد پروفیل "
            dic["location"] = "محل قرار گیری فاضلاب رو"
            dic["description"] = "توضیحات"
            dic["username"] = "نام تکمیل کننده"
            dic["munRegion"] = "منطقه شهرداری"
            dic["date"] = "تاریخ برداشت اطلاعات"
            dic["time"] = "ساعت برداشت اطلاعات"
            dic["address"] = "آدرس"
            dic["manholeCode"] = "کد منهول"
            dic["longitude"] = "X"
            dic["latitude"] = "Y"
            dic["altitude"] = "Z"
            return dic
        }
    }


}