package com.idea.abfa.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "addresses_table")
class Address(@ColumnInfo(name = "main_street") val mainStreet: String) : Serializable {

    @PrimaryKey(autoGenerate = true)
    var addressId: Int = 0

    @ColumnInfo(name = "sub_street")
    var subStreet: String = ""
    @ColumnInfo(name = "alley")
    var alley: String = ""
    @ColumnInfo(name = "blind_alley")
    var blindAlley: String = ""


}