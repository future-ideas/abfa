package com.idea.abfa.models

import androidx.room.*
import java.io.Serializable


@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
@Entity(tableName = "manholes_table")
data class Manhole(
        var frameKeeper: String = "",
        var gateFrameStatus: String = "",
        var needRestoration: String = "",
        var rainPenetration: String = "",
        var stairs: String = "",
        var stairType: String = "",
        var gateStatus: String = "",
        var holesNumber: String = "",
        var stairNumber: String = "",
        var drop: String = "",
        var broken: String = "",
        var holes: String = "",
        var canalStatus: String = "",
        var muscleStatus: String = "",
        var stairsStatus: String = "",
        var photoName : String = "",
        var faultyNumbers: String = "",
        var normalBranchesNumber: String = "",
        var nonRegularBranchesNumber: String = "",
        var sewerDepth: String = "",
        var sewerLinesNumber: String = "",
        var needsToPlaster: String = "",
        var concreteInsideStatus: String = "",
        var heavyFailure: String = "",
        var smellStatus: String = "",
        var flowStatus: String = "",
        var vermin: String = "",
        var verminNames: String = "",
        var rootPenetration: String = "",
        var waterPenetration: String = "",
        var sediments: String = "",
        var description: String = "",
        var obstacle: String = "",
        var facilities: String = "",
        var diameter: String = "",
        var manholeCode: String = "",
        var gateLongitude: String = "",
        var gateLatitude: String = "",
        var gateAltitude: String = "",
        var depth: String = "",
        var typeClass: String = "",
        var crossForm: String = "",
        var specialCrossType: String = "",
        //جنس منهول بتنی یا ...
        var type: String = "",
        var location: String = "",
        var traffic: String = "",
        //نوع دریچه
        var gateClass: String = "",
        //جنس دریچه
        var gateType: String = "",
        //نوع منهول معمولی یا مخصوص
        var gateTypeClass: String = "") : Serializable {


    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    @ColumnInfo(name = "username")
    var username: String = ""
    @ColumnInfo(name = "user_id")
    var userId: String = ""

    @ColumnInfo(name = "mun_region")
    var munRegion: String = ""
    var date: String = ""
    var time: String = ""
    @Embedded
    var address: Address? = null


    fun getAddress(): String {
        return address?.mainStreet + "/" + address?.subStreet + "/" + address?.alley + "/" + address?.blindAlley
    }

    override fun toString(): String {
        return "منهول با کد $manholeCode در منطقه $munRegion"
    }

    companion object {
        fun dic(): HashMap<String, String> {
            val dic = HashMap<String, String>()
            dic["username"] = "نام تکمیل کننده"
            dic["munRegion"] = "منطقه شهرداری"
            dic["date"] = "تاریخ برداشت اطلاعات"
            dic["time"] = "ساعت برداشت اطلاعات"
            dic["address"] = "آدرس"
            dic["manholeCode"] = "کد استقرار"
            dic["gateLongitude"] = "X"
            dic["gateLatitude"] = "Y"
            dic["gateAltitude"] = "Z"
            dic["depth"] = "عمق کف آبروی در منهول"
            dic["typeClass"] = "نوع منهول"
            dic["crossForm"] = "سطح مقطع منهول"
            dic["specialCrossType"] = "سطح مقطع اتاقک منهولهای خاص"
            dic["type"] = "جنس منهول"
            dic["location"] = "مکان منهول"
            dic["traffic"] = "شدت بار  ترافیکی روی منهول"
            dic["gateType"] = "جنس دریچه"
            dic["gateTypeClass"] = "کلاس دریچه"
            dic["frameKeeper"] = "مصالح نگهدارنده دور قاب منهول"
            dic["gateFrameStatus"] = "وضعیت مصالح نگهدارنده قاب منهول"
            dic["needRestoration"] = "نیاز به بازسازی"
            dic["rainPenetration"] = "امکان ورود باران"
            dic["stairs"] = "پلکان"
            dic["stairType"] = "جنس پلکان"
            dic["gateStatus"] = "وضعیت دریچه"
            dic["holesNumber"] = "تعداد سوراخ دریچه"
            dic["stairNumber"] = "تعداد پله"
            dic["drop"] = "وجود دراپ"
            dic["broken"] = "شکستگی سازه منهول"
            dic["canalStatus"] = "وضعیت آبرو"
            dic["muscleStatus"] = "وضعیت آبرو"
            dic["stairsStatus"] = "وضعیت پلکان"
            dic["faultyNumbers"] = "تعداد کاستی پله"
            dic["normalBranchesNumber"] = "تعداد انشعابات مشترکین نصب شده اصولی"
            dic["nonRegularBranchesNumber"] = "تعداد انشعابات مشترکین نصب شده غیراصولی"
            dic["sewerDepth"] = "عمق کف آبروی منهول"
            dic["sewerLinesNumber"] = "تعداد خطوط فاضلاب"
            dic["needsToPlaster"] = "نیاز جدار داخلی  به پلاستر کشی"
            dic["concreteInsideStatus"] = "وضعیت سطح داخلی منهول بتنی"
            dic["heavyFailure"] = "خرابی محل اتصال"
            dic["smellStatus"] = "وضعیت بو"
            dic["flowStatus"] = "وضعیت جریان"
            dic["vermin"] = "نوع جانوران موذی"
            dic["verminNames"] = "نام جانوران موذی"
            dic["rootPenetration"] = "نفوذ ریشه گیاه در منهول"
            dic["waterPenetration"] = "نفوذ  آب به داخل منهول"
            dic["sediments"] = "رسوبات در آبرو"
            dic["description"] = "توضیحات"
            dic["obstacle"] = "وجود مانع در فاضلاب رو"
            dic["facilities"] = "عبور سایر تاسیسات شهری از منهول"
            dic["diameter"] = "قطر دریچه"
            dic["photoName"] = "نام تصویر منهول"
            return dic
        }
    }


}