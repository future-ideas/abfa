package com.idea.abfa

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.idea.abfa.adapters.ManholesAdapter
import com.idea.abfa.db.MyRoomDB
import kotlinx.android.synthetic.main.activity_manholes.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ManholesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val context = this
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manholes)
        //Retrieve list of manholes of user which are in file
        GlobalScope.launch{
            val xx = MyRoomDB.getDatabase(context).manholeDAO()
            val manholes = xx.getManholes() as ArrayList
            Log.i("TAG", "${manholes.size}")
            runOnUiThread {
                val adapter = ManholesAdapter(context, manholes)
                manholes_recycler.adapter = adapter
                manholes_recycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            }
        }

    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
